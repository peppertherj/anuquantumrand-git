
<project name="ANU Quantum RNG" default="jar" basedir=".">
    <description>
        ANU Quantum Random Number Generator library ant build script.
    </description>
    <property file="build.properties"/>

    <path id="classpath">
        <pathelement location="${build.dir}"/>
        <fileset dir="${lib.dir}" includes="**/*.jar"/>
    </path>

    <path id="compile.classpath">
        <path refId="classpath"/>
        <pathelement location="${src.dir}"/>
    </path>

    <target name="init" depends="mkdir">
        <tstamp>
            <format property="jar.time" pattern="yyyy-MM-dd'T'HH:mm:ssZ'Z'"/>
        </tstamp>
    </target>
    
    <target name="mkdir">
        <mkdir dir="${build.dir}"/>
        <mkdir dir="${jar.dir}"/>
        <mkdir dir="${doc.dir}"/>
        <mkdir dir="${src.dir}"/>
    </target>

    <target name="clean">
        <delete dir="${build.dir}"/>
        <delete dir="${jar.dir}"/>
        <delete dir="${doc.dir}"/>
    </target>

    <target name="compile" depends="init">
        <echo level="info">Starting compiler: ${jar.time}</echo>
        <mkdir dir="${build.dir}"/>
        <javac classpathref="compile.classpath" srcdir="${src.dir}"
               destdir="${build.dir}" includeAntRuntime="no" failonerror="true"
               debug="true"
               debuglevel="lines,vars,source">
        </javac>
        <copy todir="${build.dir}">
            <fileset dir="${src.dir}" excludes="**/*.java"/>
        </copy>
    </target>

    <target name="document" depends="init">
        <javadoc sourcepath="${src.dir}" classpathref="compile.classpath" destdir="${doc.dir}"
                 windowtitle="ANU Quantum Random Number Generator API" doctitle="ANU Quantum Random Number Generator API">
			<!-- Put links to java 6 API into the java docs (So things like java.lang.String aren't littered everywhere -->
            <link href="http://download.oracle.com/javase/6/docs/api/"/>
        </javadoc>
    </target>
    
    <target name="doc" depends="document"/>
        
    <target name="jar" depends="compile,document">
        <!-- Builds a jar containing all binary (.class) files -->
        <jar basedir="${build.dir}" destfile="${jar.dir}/${lib.name}-${version}-bin.jar">
            <manifest>
                <attribute name="Built-By" value="${user.name}"/>
                <attribute name="Built-On" value="${jar.time}"/>
            </manifest>
        </jar>
        <!-- Builds a jar containing all documentation files -->
        <jar basedir="${doc.dir}" destfile="${jar.dir}/${lib.name}-${version}-doc.jar">
            <manifest>
                <attribute name="Built-By" value="${user.name}"/>
                <attribute name="Built-On" value="${jar.time}"/>
            </manifest>
        </jar>
        <!-- Builds a jar containing all source files -->
        <jar basedir="${src.dir}" destfile="${jar.dir}/${lib.name}-${version}-src.jar">
            <manifest>
                <attribute name="Built-By" value="${user.name}"/>
                <attribute name="Built-On" value="${jar.time}"/>
            </manifest>
        </jar>
        <!-- Builds a jar containing all binaries, source files, and documentation -->
        <jar basedir="." jarfile="${jar.dir}/${lib.name}-${version}-full.jar" excludes="**/**">
			<!-- Include the entire source and javadoc directories in the jar file-->
            <fileset dir="." includes="${src.dir}/**"/>
            <fileset dir="." includes="${doc.dir}/**"/>
			<!-- Put everything in the build directory in the root of the jar file -->
            <fileset dir="${build.dir}" includes="**/**"/>
            <manifest>
                <attribute name="Built-By" value="${user.name}"/>
                <attribute name="Built-On" value="${jar.time}"/>
            </manifest>
        </jar>
    </target>
    
    <target name="dist" depends="jar"/>
</project>
