package org.anuqrng.generation;

import java.io.IOException;

/**
 *
 * @author Brian
 */
interface ByteFetcher {

    byte nextByte() throws IOException;

    byte[] nextBytes(int count) throws IOException;
}
